(use ['clojure.string :only '(split)])

(def input (split (slurp "./input") #"\n"))

(defn- parse-param
  [n]
  (try 
    (Integer/parseInt n) 
    (catch NumberFormatException _ 
      (keyword n))))

(defn- parse-op
  [o]
  (case o
    "AND" bit-and
    "OR"  bit-or
    "NOT" bit-not
    "RSHIFT" bit-shift-right
    "LSHIFT" bit-shift-left))

(defn parse
  ([a op b] 
   [(parse-op op) (parse-param a) (parse-param b)])
  ([op a] 
   [(parse-op op) (parse-param a)])
  ([a] 
   [(parse-param a)]))

(defn build
  [line]
  (let [[f _ & args] (reverse (split line #" "))]
    (do
      ;(println "f" f)
      ;(println "as" args)
      [(keyword f) (apply parse (reverse args))])))

(def a "lf AND lq -> ls")
;(println (build a))

(defn build-all
  [lines]
  (into {} (map build lines)))

;(println (count (keys (build-all input))))

(declare wires)

; heh, 29min and no end without memoize...
; add memoize? bam! 1.4 seconds!!!
(def resolve-key
  (memoize (fn [k]
  (if (and (not (fn? k)) (not (keyword? k)))
    k
    (let [v (k wires)
          f (first v)]
    (if (fn? f)  
      (bit-and 0xffff 
        (apply f 
          (map resolve-key (rest v))))
      (resolve-key f)))))))

;(def few (build-all '("123 -> x" "456 -> y" "x AND y -> d" "NOT x -> h")))

;(println (resolve-key few :x))
;(println (resolve-key few :y))
;(println (resolve-key few :d))
;(println (resolve-key few :h))

;(def less (build-all '("123 -> x" "NOT x -> h")))
;(println (resolve-key less :h))

;(def less (build-all '("123 -> x" "1 AND 5 -> h")))
;(println (resolve-key less :h))

;(println (build "NOT x -> h"))

;(def the-test (build-all (split (slurp "./test") #"\n")))
;(println "d:" (resolve-key the-test :d))
;(println "e:" (resolve-key the-test :e))
;(println "f:" (resolve-key the-test :f))
;(println "g:" (resolve-key the-test :g))
;(println "h:" (resolve-key the-test :h))
;(println "i:" (resolve-key the-test :i))
;(println "x:" (resolve-key the-test :x))
;(println "y:" (resolve-key the-test :y))

;(def wires (build-all input))

;(println (resolve-key all :b))
; 16076
;(println (resolve-key :a))
;(def wires (build-all (split (slurp "./input.part2") #"\n")))
; 2797
;(println (resolve-key :a))
