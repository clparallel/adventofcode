(use ['clojure.string :only '(split)])

(defn area
  [l w h]
  (+
    (* 2 l w)
    (* 2 w h)
    (* 2 h l)))

(defn smallest-face
  [l w h]
  (take 2 
    (sort [l w h])))

(defn slack
  [l w h]
  (apply *
    (smallest-face l w h)))

(defn wrapping
  [l w h]
  (+ 
    (area l w h)
    (slack l w h)))

; 43
; (println
;   (wrapping 1 1 10))

; 58
; (println
;   (wrapping 2 3 4))

(defn parse
  [lines]
  (for
    [line lines]
    (map #(Integer/parseInt %) line)))

(def input 
  (parse 
    (map 
      #(split % #"x") 
      (split (slurp "./input") #"\n"))))

(defn total-wrapping
  [dims]
  (apply +
    (map #(apply wrapping %) 
      dims)))

(defn total
  [dims f]
  (apply +
    (map #(apply f %) 
      dims)))

(defn total-wrapping
  [dims]
  (total dims wrapping))

; 1586300
(println 
  (total-wrapping input))

(defn ribbon-wrap
  [l w h]
  (apply +
    (map #(* 2 %)
      (smallest-face l w h))))

(defn ribbon-bow
  [l w h]
  (* l w h))

(defn ribbon
  [l w h]
  (+
   (ribbon-wrap l w h)
   (ribbon-bow l w h)))

; 34
; (println (ribbon 2 3 4))

; 14
; (println (ribbon 1 1 10))

(defn total-ribbon
  [dims]
  (total dims ribbon))

; 3737498
;(println 
;  (total-ribbon input))
