(def input 
  (map #(cond 
    (= % \() 1
    (= % \)) -1)
    (seq (slurp "./input"))))

;280
(println
 (reduce + 0 input))

;1797
(println
 (reduce 
   (fn [acc x] 
     (if (< (:sum acc) 0)
       (reduced acc)
       (assoc acc
         :sum (+ (:sum acc) x) 
         :idx (inc (:idx acc)))))
   {:sum 0 :idx 0}
   input))
