; set of coord pairs
; sort by .. uh, a set
; taking present (x, y) = +/- => (x, y)

(defn move
  [curr edge]
  [(+ (first curr) (first edge))
   (+ (second curr) (second edge))])

; [-1 1]
;(println
;   (move [0, 1] [-1, 0]))

(defn walk
  [start edges]
  (loop [res edges
         path (list start)]
    (let [cur (first path)
          nxt (first res)]
    (if (nil? nxt)
      (reverse path)
      (recur
        (rest res) 
        (cons (move cur nxt) path))))))

; ([0 1] [0 2] [1 2] [0 2] [0 1])
;(println
;  (walk [0,0] 
;    '([0,1] [0, 1] [1,0], [-1,0], [0,-1])))

(defn count-unique
  [homes]
  (count (set homes)))

(defn convert
  [d]
  (case d
    \> [1, 0]
    \^ [0, 1]
    \< [-1, 0]
    \v [0, -1]))

(def input
  (map convert (slurp "./input")))

; 2591
(println
  (count-unique
    (walk [0,0] input)))

(defn split-indexed
  [in f]
  (map #(second %)
    (filter #(f (first %)) 
      (map vector 
         (iterate inc 0) 
         in))))

(def inputA
  (split-indexed input even?))

(def inputB
  (split-indexed input odd?))

; t 8192
; a: 4096
; b: 4096
;(do
;  (println "t" (count input))
;  (println "a:" (count inputA))
;  (println "b:" (count inputB)))

; 2360
(println
  (count-unique
    (concat
      (walk [0,0] inputA)
      (walk [0,0] inputB))))
