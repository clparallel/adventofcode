; thanks https://gist.github.com/jizhang/4325757 !
(import 'java.security.MessageDigest
        'java.math.BigInteger)

(defn md5 [s]
  (let [algorithm (MessageDigest/getInstance "MD5")
        size (* 2 (.getDigestLength algorithm))
        raw (.digest algorithm (.getBytes s))
        sig (.toString (BigInteger. 1 raw) 16)
        padding (apply str (repeat (- size (count sig)) "0"))]
    (str padding sig)))

; (println (md5 "abc"))

(declare secret)

(def secret "ckczppom")

(defn mine-hashes [s start-match]
  (first
    (filter #(.startsWith (:hash %) start-match)
	(map
	  (fn [idx]
		(assoc {}
		  :hash (md5 (str s idx))
		  :indx idx))
      (iterate inc 1)))))

; 117946
(println (:indx 
  (mine-hashes secret "00000")))

; 3938038
(println (:indx 
  (mine-hashes secret "000000")))
