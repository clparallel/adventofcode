(use ['clojure.set]
	 ['clojure.string :only '(split)])

(defn split-pairwise
  ;lol wut is this
  ([word] (split-pairwise word 2))
  ([word n]
    (map #(apply str %) (partition n 1 (vec word)))))

(defn double-letter?
  ([word] (double-letter? word first second 2))
  ([word a b n]
  (let [cs (split-pairwise word n)]
    (not (nil? (first
      (filter #(= (a %) (b %)) (set cs))))))))

;(println (split-pairwise "ayayayaya"))
;(println (split-pairwise "ayayayaya" 3))
;true
;(println (double-letter? "aa"))
;false 
;(println (double-letter? "axa"))
;true
;(println (double-letter? "axxa"))
;true
;(println (double-letter? "aya" first last))
;(println (double-letter? "aaa" first last))
;(println (double-letter? "ayya" first last))
;false
;(println (double-letter? "ayb" first last))

(declare vowels)
(def vowels
  #{\a \e \i \o \u})

(defn three-vowels?
  ([word] (three-vowels? word vowels))
  ([word vs]
    (let [cs (vec word)]
      (<= 3 (count (filter #(contains? vs %) cs))))))

; true
;(println (three-vowels? "aaaaa"))
; false
;(println (three-vowels? "aa"))
; true
;(println (three-vowels? "aie"))
; true
;(println (three-vowels? "axixe"))

(declare restricted)
(def restricted
  #{"ab" "cd" "pq" "xy"})

;true
;zaabc
;(println (contains? #{"za" "aa" "ab" "bc"} "aa"))

(defn contains-restricted?
  ([word] (contains-restricted? word restricted))
  ([word bad]
    (let [cs (split-pairwise word)]
      (not (empty?
         (clojure.set/intersection (set cs) bad))))))

; true
;(println (contains-restricted? "abcdpqxy"))
; false
;(println (contains-restricted? "aaaa"))

(defn nice?
  [word]
  (and 
    (double-letter? word)
    (three-vowels? word)
    (not (contains-restricted? word))))

(defn naughty?
  [word]
  (not (nice? word)))

;(assert (nice? "ugknbfddgicrmopn"))
;(assert (nice? "aaa"))
;(assert (naughty? "jchzalrnumimnmhp"))
;(assert (naughty? "haegwjzuvuyypxyu"))
;(assert (naughty? "dvszwmarrgswjxmb"))

; true
;(println (nice? "ugknbfddgicrmopn"))
; true
;(println (naughty? "jchzalrnumimnmhp"))

(def input
  (split (slurp "./input") #"\n"))

(defn count-nice
  [in]
  (count (filter nice? in)))

; 238
(println (count-nice input))

(defn sandwiched-letters?
  [word]
  (double-letter? word first last 3))

;true
;(println (sandwiched-letters? "aya"))
;(println (sandwiched-letters? "aaa"))
;false
;(println (sandwiched-letters? "ayya"))
;(println (sandwiched-letters? "ayb"))

; non-overlapping pairs appearing at least twice
(defn two-pairs?
  [word]
  (let [cs (split-pairwise word)
        search (map #(nthrest cs %) (range (count cs)))]
    (not (empty? (filter #(some (partial = (first %)) (rest (rest %))) search)))))

;true
;(println (two-pairs? "aaaa"))
;(println (two-pairs? "aabaa"))
;(println (two-pairs? "qjhvhtzxzqqjkmpb"))
;false
;(println (two-pairs? "aaa"))
;(println (two-pairs? "abbaa"))

(defn nicer?
  [word]
  (and
    (sandwiched-letters? word)
    (two-pairs? word)))

;true
;(println (nicer? "qjhvhtzxzqqjkmpb"))
;(println (nicer? "xxyxx"))
;false
;(println (nicer? "uurcxstgmygtbstg"))
;(println (nicer? "ieodomkazucvgmuy"))

(defn count-nicer
  [in]
  (count (filter nicer? in)))

; 69
(println (count-nicer input))
