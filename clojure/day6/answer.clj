(use ['clojure.set]
     ['clojure.string :only '(split)])

(defn choices
  [xs]
  (if (empty? xs)
    '(())
    (for [one (first xs)
          else (choices (rest xs))]
      (cons one else))))

(defn coords
  ([cs]
    (let [[x y x' y'] cs]
      (coords x y x' y')))
  ([x y x' y']
  (list 
    (range x (inc x')) 
    (range y (inc y')))))

(defn list-coords
  ([cs] (choices (coords cs)))
  ([x y x' y'] (choices (coords x y x' y'))))

;((0 0) (0 1) (0 2) (1 0) (1 1) (1 2) (2 0) (2 1) (2 2))
;(println (list-coords '(0 0 2 2)))

(defn turn
  ([f x y x' y' existing]
   (turn f (set (list-coords x y x' y')) existing))
  ([f cs existing]
   (f existing cs)))

(defn on
  ([x y x' y' existing] (turn clojure.set/union x y x' y' existing))
  ([cs existing] (turn clojure.set/union cs existing)))

(defn off
  ([x y x' y' existing] (turn clojure.set/difference x y x' y' existing))
  ([cs existing] (turn clojure.set/difference cs existing)))

(defn toggle
  ([x y x' y' existing] (toggle (set (list-coords x y x' y')) existing))
  ([for-toggle existing]
  (let [for-off (clojure.set/intersection existing for-toggle)
		for-on (clojure.set/difference for-toggle for-off)]
      (on for-on (off for-off existing)))))

;#{(0 0) (0 2) (0 1)}
;(println (on 0 0 0 2 #{}))
;(println (on '(0 0 0 2) #{}))
;#{(0 0)}
;(println (off 0 1 0 2 #{'(0 0) '(0 1) '(0 2)}))
;#{(0 0) (0 3) (0 1)}
;(println (toggle 0 2 0 3 #{'(0 0) '(0 1) '(0 2)}))

(defn parse-coords
  [s]
  (map #(Integer/parseInt %) (split s #",")))

(defn part1
  [op]
  (case op "on" on "off" off "toggle" toggle))

(defn choose-method
  [chooser line]
  (let [entries (reverse (split line #" "))
        prime (parse-coords (first entries))
        orig (parse-coords (first (drop 2 entries)))
        [x y x' y'] (flatten [orig prime])
        op (first (drop 3 entries))
        f (chooser op)]
    (partial f x y x' y')))

;#{(2 2) (0 0) (1 0) (1 1) (0 2) (2 0) (2 1) (1 2) (0 1)}
;(println ((choose-method "turn on 0,0 through 2,2") #{}))

(defn set-lights
  [lines choose c]
  (reduce #((choose-method choose %2) %1) c lines))

;#{(4 3) (2 2) (0 0) (1 0) (3 3) (1 1) (3 4) (0 2) (2 0) (2 1) (4 4) (1 2) (0 1)}
;(println (set-lights '("turn on 0,0 through 2,2" "turn on 3,3 through 4,4")))

(def input (split (slurp "./input") #"\n"))

(defn count-lights
  [lines]
  (count (set-lights lines part1 #{})))

; 569999
; (println (count-lights input))

;1000
;(println (count-lights '("toggle 0,0 through 999,0")))

(defn floor-add
  [a b]
  (let [r (+ a b)]
    (if (< r 0)
      0
      r)))

;(println (floor-add 0 -1))
;(println (floor-add -1 -10))
;(println (floor-add -1 10))
;(println (floor-add 0 0))

(defn guard
  [e]
  (let [[k v] e]
    (if (< v 0) 
       [k 0]
       [k v])))

;(println (guard '((1 2) 0)))
;(println (guard '((1 2) -1)))

(defn adjust
  ([n x y x' y' existing]
   (let [m (zipmap (list-coords x y x' y') (repeat n))]
   (adjust m existing)))
  ([cs existing]
    (into {} 
      (map guard (seq (merge-with floor-add existing cs))))))

;(println (flatten (map guard (seq (merge-with floor-add {} {})))))
;(println (map guard (seq (merge-with floor-add {'(0 1) -1} {'(1 0) 1}))))
;(println (into {} (map guard (seq (merge-with floor-add {'(0 1) -1} {'(1 0) 1})))))
;(def a (partial adjust 1))
;(def b (partial a 0 0 1 1))
;(def z (let [m (zipmap (list-coords 0 0 1 1) (repeat 1))] (adjust m {})))
;(def z (apply #(partial a %) (zipmap (list-coords 0 0 1 1) (repeat 1))))
;(println (b {}))
;(println z )

(defn part2
  [op]
  (case op "on" (partial adjust 1) "off" (partial adjust -1) "toggle" (partial adjust 2)))

;(println (choose-method part2 "turn on 0,0 through 2,2"))
;(println (set-lights '("turn on 0,0 through 2,2" "turn on 3,3 through 4,4") part2 {}))
;(println (set-lights '("turn on 0,0 through 2,2" "turn off 0,0 through 4,4") part2 {}))

(defn intensity-lights
  [lines]
  (apply + (vals (set-lights lines part2 {}))))

; after much sweating and consternation by compy...
; 17836115
;(println (intensity-lights input))
