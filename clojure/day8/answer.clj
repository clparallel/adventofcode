(require '[clojure.string :as string])

(def input (string/split (slurp "./input") #"\n"))

(defn memory
  [line]
  (-> line
    (string/replace "\\\\" "S")
    (string/replace "\\\"" "Q")
    (string/replace #"\\x[a-f0-9]{2}" "Z")))

(defn diff
  [line]
  (- (count line) (- (count (memory line)) 2)))

(defn answer
  [lines]
  (reduce + (map diff lines)))

;(println (memory "abc"))
;(println (map memory (string/split (slurp "./test") #"\n")))
; 1342
(println (answer input))

(defn double-memory
  [line]
  (-> line
    (string/replace "\\" "\\\\")
    (string/replace "\"" "\\\"")))

(defn diff2
  [line]
  (- (+ (count (double-memory line)) 2) (count line)))

(defn answer2
  [lines]
  (reduce + (map diff2 lines)))

(println (answer2 input))
